/* There is some stuff.. */

void loadsprite(struct programstate *psp)      // TO IMPROOVE..............
{
   char str[64];

   psp->sprite.fps = 8;
   psp->sprite.frames = 8;
   int i;
   for(i=0; i<psp->sprite.frames; ++i)
   {
      sprintf(str,"gfx\\tiles\\bluesea%i.bmp",i);
      psp->sprite.frame[i] = al_load_bitmap(str);
      if(!psp->sprite.frame[i])
      {
         printf("Horrible error - tile [%i] not loaded\n",i);
         psp->sprite.frame[i] = al_create_bitmap(TILE_DIMENSIONS,TILE_DIMENSIONS);
      }
   }
   printf("loadsprite(): bluesea...... LOADED\n");
}

void loadtilemap(struct programstate *psp)   // load tile map as bitmap DONE
{
   int i,j;
   int x,y;
   unsigned char r,g,b;
   ALLEGRO_BITMAP *tilebitmap;

   tilebitmap = al_load_bitmap( psp->mapfile );
   if(!tilebitmap)
   {
      tilebitmap = al_load_bitmap( TILEMAP_FILE );
      if(!tilebitmap)
         return;
   }
   else
   {
      psp->tilemap.height = y = al_get_bitmap_height(tilebitmap);
      psp->tilemap.width = x = al_get_bitmap_width(tilebitmap);

      psp->displacement.x = -(x/2);   // working on it now
      psp->displacement.y = -(y/2);

      psp->tilemap.tile = malloc((sizeof *(psp->tilemap.tile))*x*y);
      printf("Allocated memory %i bytes of memory for %i X %i tilemap\n",(sizeof *(psp->tilemap.tile))*x*y,x,y);

      //loadtilemap from bitmap...
      for(i=0; i<y; ++i)
      {
         for(j=0; j<x; ++j)
         {
            al_unmap_rgb(al_get_pixel(tilebitmap, j, i), &r, &g, &b);
            if(r==255 && g==255 && b==0)     // YELLOW
               psp->tilemap.tile[i*x+j] = 1;
            else if(r==0 && g==255 && b==0)  // GREEN
               psp->tilemap.tile[i*x+j] = 2;
            else if(r==0 && g==0 && b==255)  // BLUE
               psp->tilemap.tile[i*x+j] = 3;
            else
               psp->tilemap.tile[i*x+j] = 0;  // OTHER
         }
      }
   }
   al_destroy_bitmap(tilebitmap);  // bitmap content converted to better format, discarding
   return;
}

void drawsprite(struct programstate *psp, struct gamestate *gs, float x, float y, short type)  // ...............Repair it..or not
{
   static unsigned int fr;
   int absframe = gs->cycle*psp->sprite.fps*2/FPS;

   fr =  absframe%psp->sprite.frames;
   switch (type)
   {
      case 0:
          // cyber void
         break;
      case 1:
        al_draw_bitmap(psp->yelldes, x, y, 0);
        break;
      case 2:
        al_draw_bitmap(psp->grgrass, x, y, 0);
        break;
      case 3:
        al_draw_bitmap(psp->sprite.frame[fr], x, y, 0);
        break;
      default:
        al_draw_bitmap(psp->noacces, x, y, 0);
        break;
   }
}

void heniekthetiler(struct programstate *psp, struct gamestate *gs)     // draw tilemap centered-ish
{
   int x,y;  // top leftonscreen tile
   short type;
   int maxx,maxy;

   maxx = (int)ceil((gs->camx+psp->displaywidth)/TILE_DIMENSIONS);  //maximum tiled map
   maxy = (int)ceil((gs->camy+psp->displayheight)/TILE_DIMENSIONS);

   y = (int)floor((gs->camy)/TILE_DIMENSIONS);
   for(;y<=maxy;++y)
   {
      x = (int)floor((gs->camx)/TILE_DIMENSIONS);
      for(;x<=maxx;++x)
      {
         if(x >= psp->displacement.x && x < psp->tilemap.width + psp->displacement.x && y >= psp->displacement.y && y < psp->tilemap.height + psp->displacement.y)
            type = psp->tilemap.tile[(y-psp->displacement.y)*psp->tilemap.width+x-psp->displacement.x];
         else
           type = 4;
         drawsprite(psp,gs,x*TILE_DIMENSIONS-gs->camx,y*TILE_DIMENSIONS-gs->camy,type);
      }
   }
   return;
}

void drawminimap(struct programstate *psp, struct gamestate *gsp)
{
  ;// draw minimap

}

void readparams(char **tab, int cnt,struct programstate *psp)
{
  int i;
  FILE *f = NULL;

  for(i=0;i<cnt;++i)
  {
     if(strcmp(tab[i],"map") == 0)
     {
       if(++i < cnt)
         sprintf(psp->mapfile,"maps\\%s",tab[i]);
     }
     else if(strcmp(tab[i],"fullscreen") == 0)
       psp->fullscreen = true;
  }
}

unsigned char gettiletype(struct pointt position, struct programstate *psp)
{
   int x,y;

   x = (int)floor(position.x/TILE_DIMENSIONS);
   y = (int)floor(position.y/TILE_DIMENSIONS);


   if(x >= psp->displacement.x && x < psp->tilemap.width + psp->displacement.x && y >= psp->displacement.y && y < psp->tilemap.height + psp->displacement.y)
      return psp->tilemap.tile[(y-psp->displacement.y)*psp->tilemap.width+x-psp->displacement.x];
   else
      return 0;
}
