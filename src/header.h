/*
    Project Jet Bomber by Robert Łyko
    header.h file contains #include, #define calls and structure definitions
*/

#include <stdio.h>
#include <math.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#define DISPLAY_W 1200
#define DISPLAY_H 800
#define DISPLAY_WF 1600
#define DISPLAY_HF 900
#define FPS 120.0

#define BOMBER_RADIUS 10.0
#define ROTSPD 120.0
#define ACCELERATION 200.0
#define MAXVEL 1000.0
#define MINVEL 0.0
#define MAX_BOMBS 10
#define EXPLOSION_RADIUS 200
#define BOMB_RATE 4.0

#define MAX_TARGETS 100
#define MAX_ENEMYS 100
#define MAX_BULLETS 3000
#define FIRE_RATE 10.0
#define TURRET_ROTSPD 180.0
#define MISSILE_VELOCITY 400

#define MAX_SPRITE_FRAMES 8
#define TILE_DIMENSIONS 256

#define MAX_NAME_LENGHT 16
#define CFG_FILE "mem.cfg"
#define TILEMAP_FILE "maps\\island.bmp"

struct pointt{
    float x;
    float y;
};

struct coord{
    int x;
    int y;
};

struct spritet{
    struct pointt pos;
    int frames;
    ALLEGRO_BITMAP *frame[MAX_SPRITE_FRAMES];
    int fps;
};

typedef struct tmap{
    int width;
    int height;
    unsigned char* tile;
};

typedef struct scoreunit{
    int score;
    char name[MAX_NAME_LENGHT];
}scoreunit;

struct bombt{
    struct pointt pos;
    float dir;
    float speed;
    int cycleleft;
};

struct bullett{
    struct pointt pos;
    float dir;
    int cycleleft;
};

struct xpolosion{
    struct pointt pos;
    int cycleleft;
};

struct targett{
    struct pointt pos;
    int life;
    int type;
};

struct enemyt{
    struct pointt pos;
    float dir;
    bool enable;
    int life;
    int reloading;
    int type;
};

struct gamestate        // game state structure
{
    int cycle;           // game loop cycle counter
    double camx;         // camera x position
    double camy;         // camera y position

    struct pointt ppos;  // player position x & y
    double dir;          // plane direction
    double spd;          // plane speed
    int energy;          // plane armor
    int points;          // points
    bool playerdead;     // is the player dead
    bool playerwon;      // did player cleared the stage?
    int govercycle;      // game cycle at which player died or cleared stage

    bool scoreregistered;// score already registered. do not input anymore

    struct bombt bomb[MAX_BOMBS]; // bombs
    struct xpolosion xplod[MAX_BOMBS]; // explode
    int bombcount;       // how many bombs is there
    int bombdelay;       // delay between bomb drop
    int bombsdropped;    // counter of  bombs dropped during game

    int targetsleft;
    int enemysleft;      // enemies still alive

    bool drain;           // drain energy?
    bool ignoreplayer;
    bool ghostmode;       // aka take no damage
    bool nastymode;

    bool drawhud;            // draw all head up display
    bool showpointer;       // navigation help
    bool showcrosshair;     // crosshair
    bool showdebugbar;      // that top textbar
    bool autorelease;       // automatic bomb realease

    struct targett target[MAX_TARGETS]; // array of targets
    struct enemyt enemy[MAX_ENEMYS];    // array of enemies
    struct bullett bullet[MAX_BULLETS]; // all the bullets
    int bulletcount;                    // existing bullets amount

};

struct programstate      // program state structure
{
    int displaywidth;
    int displayheight;
    int lastpos;         // last position in score
    bool quit;           // main loop breaker
    bool stngame;        // start new game
    bool ingame;         // is game sunning
    int cycle;           // main loop cycle counter
    int window;          // actual window state
    int eventCounter;    // counts events processed in single cycle
    bool keys[ALLEGRO_KEY_MAX]; // char keys pressed
    char playername[MAX_NAME_LENGHT]; // playername
    bool kp[6];          // key press
    char mapfile[32];    // mapname
    bool fullscreen;     // run in fullscreen

    bool showbombstatus;     // show bomb info
    bool showbulletstatus;   // show bullet info
    bool showtargetdebug;    // show target data
    bool showenemydebug;     // show enemy debug

    ALLEGRO_COLOR bgcolor;
    ALLEGRO_COLOR indicated;
    ALLEGRO_COLOR disabled;
    ALLEGRO_COLOR def;
    ALLEGRO_FONT *dfont;
    ALLEGRO_FONT *font;
    ALLEGRO_FONT *font96;
    ALLEGRO_FONT *font64;
    ALLEGRO_FONT *font48;

    ALLEGRO_BITMAP *displayBuffer, *minimap;

    ALLEGRO_BITMAP *bomber, *bomberwreck, *bomb, *target, *dtarget, *targetruin, *dtargetruin, *enemy, *enemywreck, *bullet, *base, *boat;
    ALLEGRO_SAMPLE *bombaway, *shoot, *boom, *hitsound;

    ALLEGRO_KEYBOARD_STATE kstate;  // keyboard snap shoot
    ALLEGRO_KEYBOARD_STATE *kb;     // pointer to kstate
    ALLEGRO_EVENT_QUEUE *eq;        // event queue
    ALLEGRO_TIMER *timer;           // timer

    ALLEGRO_CONFIG *cfg;            // game config pointer

    scoreunit scores[10];           // actual scores
    ALLEGRO_BITMAP *grgrass, *yelldes, *noacces;
    struct spritet sprite;
    struct tmap tilemap;
    struct coord displacement;            // displacement of tilemap topleft tile relative to [0,0] position

};

#include "declarations.h"
#include "menus.h"
#include "functions.h"
#include "stuff.h"
