/*
   *********************************  NOTICE  ***************************************

   This is genuine realistic jet bomber training software created by ingenious great incredible Robert �yko
   If You want to use this software, bear in mind that copying, distributing, modifying software or thinking
   bad about this note is strictly prohibited and may cause death, cancer, spontaneous ignition, or even
   excommunication.
   Don't copy that floppy

*/

#include "header.h"

int main(int argc, char **argv)
{
    int i;
    struct programstate ps = {0}, *psp = &ps;
    int code = 0;       //  exitcode
    ps.quit = false;
    ps.window = 1;
    ps.lastpos = -1;
    ps.kb = &(ps.kstate);
    strcpy(ps.playername,"PLAYER");
    strcpy(ps.mapfile,TILEMAP_FILE);
    readparams(argv, argc,psp);

    // initialize allegro library
    if(!al_init()) {
        fprintf(stderr, "failed to initialize allegro!\n");
        return -1;
    }

    // create display

    ALLEGRO_DISPLAY *display = NULL;
    if(ps.fullscreen)
    {
       ps.displaywidth = DISPLAY_WF;
       ps.displayheight = DISPLAY_HF;
       al_set_new_display_flags(ALLEGRO_OPENGL|ALLEGRO_FULLSCREEN_WINDOW);
       al_set_new_display_option(ALLEGRO_CAN_DRAW_INTO_BITMAP, 1, ALLEGRO_REQUIRE);
       display = al_create_display(DISPLAY_WF, DISPLAY_HF);
       if(!display)
          goto windowed;
    }
    else
    {
       windowed:
       ps.displaywidth = DISPLAY_W;
       ps.displayheight = DISPLAY_H;
       al_set_new_display_flags(ALLEGRO_OPENGL|ALLEGRO_WINDOWED);
       al_set_new_display_option(ALLEGRO_CAN_DRAW_INTO_BITMAP, 1, ALLEGRO_REQUIRE);
       display = al_create_display(DISPLAY_W, DISPLAY_H);
    }
    if(!display) {
        fprintf(stderr, "failed to create display!\n");
        code = -2;
        goto exitseq;
    }

    al_init_image_addon();

    // install keyboard
    if(!al_install_keyboard()) {
        fprintf(stderr, "failed to initialize the keyboard!\n");
        code = -3;
        goto exitseq;
    }

    ps.timer = al_create_timer(1.0 / FPS);
    if(!ps.timer) {
        fprintf(stderr, "failed to create timer!\n");
        code = -4;
        goto exitseq;
    }

    // init font addon & ttf addon
    al_init_font_addon();
    al_init_ttf_addon();

    // sound stuff
    if(!al_install_audio()){
      fprintf(stderr, "failed to initialize audio!\n");
       code = -5;
       goto exitseq;
    }

    if(!al_init_acodec_addon()){
      fprintf(stderr, "failed to initialize audio codecs!\n");
       code = -6;
       goto exitseq;
    }

   if (!al_reserve_samples(16)){
      fprintf(stderr, "failed to reserve samples!\n");
       code = -7;
       goto exitseq;
   }


    loader(psp); // load all stuff

    ps.bgcolor = al_map_rgb(0,0,0);
    ps.indicated = al_map_rgb(255,255,255);
    ps.disabled = al_map_rgb(128,128,128);
    ps.def = al_map_rgb(255,0,0);

    ps.eq = al_create_event_queue();
    al_register_event_source(ps.eq, al_get_display_event_source(display));
    al_register_event_source(ps.eq, al_get_timer_event_source(ps.timer));
    al_register_event_source(ps.eq, al_get_keyboard_event_source());

    ps.displayBuffer = al_create_bitmap(ps.displaywidth,ps.displayheight);
    al_start_timer(ps.timer);

    // main loop of program
    for(ps.cycle = 0; !ps.quit; ps.cycle++)
    {
        al_set_target_bitmap(ps.displayBuffer);              // draw to memory bitmap which is display

        switch (ps.window){
            case 1 : domenu(psp);
                break;
            case 2 : dongame(psp);
                break;
            case 3 : scoreboard(psp);
                break;
            case 4 : credits(psp);
                break;
            case 5 : game(psp);
                break;

            default : error(psp);
                break;
        }
        al_set_target_bitmap(al_get_backbuffer(display)); // draw to backbuffer of display
        al_draw_bitmap(ps.displayBuffer,0,0,0);              // copy frame to backbuffer
        al_flip_display();                                // flip

        for(i=0 ; i<ALLEGRO_KEY_MAX ; i++)           // clear characters
            ps.keys[i] = false;     // no key pressed

        while(true)
        {
            ALLEGRO_EVENT ev;

            al_wait_for_event(ps.eq, &ev);
            if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE){
                ps.quit = true;
                break;
            }else if (ev.type == ALLEGRO_EVENT_TIMER){
                break;
            }else if (ev.type == ALLEGRO_EVENT_KEY_CHAR)    // character input
            {
                ps.keys[ev.keyboard.keycode] = true;
                if (ev.keyboard.modifiers & ALLEGRO_KEYMOD_CAPSLOCK)
                    ps.keys[ALLEGRO_KEY_CAPSLOCK] = true;
                if (ev.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT)
                    ps.keys[ALLEGRO_KEY_LSHIFT] = true;
                if (ev.keyboard.modifiers & ALLEGRO_KEYMOD_ALTGR)
                    ps.keys[ALLEGRO_KEY_ALTGR] = true;
            }
        }

        //al_flush_event_queue(ps.eq);  // been error on commenting this line
        al_get_keyboard_state(ps.kb);
    }

exitseq:
    al_destroy_event_queue(ps.eq);                     // remove event queue

    al_destroy_font(ps.font);                          // font destroyer
    al_destroy_font(ps.font96);                          // font destroyer
    al_destroy_font(ps.font64);                          // font destroyer
    al_destroy_font(ps.font48);                          // font destroyer

    // DESTROY BITMAPS

    al_destroy_bitmap(ps.bomber);                       // bomber bitmap deleter
    al_destroy_bitmap(ps.bomberwreck);
    al_destroy_bitmap(ps.bomb);                         // bomb bitmap deleter
    al_destroy_bitmap(ps.target);
    al_destroy_bitmap(ps.targetruin);
    al_destroy_bitmap(ps.dtarget);
    al_destroy_bitmap(ps.dtargetruin);
    al_destroy_bitmap(ps.enemy);
    al_destroy_bitmap(ps.enemywreck);
    al_destroy_bitmap(ps.bullet);
    al_destroy_bitmap(ps.boat);
    al_destroy_bitmap(ps.base);

    // DESTROY SAMPLES

    al_destroy_sample(ps.shoot);
    al_destroy_sample(ps.bombaway);
    al_destroy_sample(ps.boom);
    al_destroy_sample(ps.hitsound);

    // DESTROY TILES
    al_destroy_bitmap(ps.grgrass);
    al_destroy_bitmap(ps.yelldes);
    al_destroy_bitmap(ps.noacces);

    al_shutdown_font_addon();                          // shutdown font addon
    al_uninstall_keyboard();                           // uninstall keyboard
    al_shutdown_image_addon();                         // uninstall image addon
    al_destroy_display(display);                       // destroy display
    al_uninstall_system();                             // uninstall allegro library
    return code;
}
