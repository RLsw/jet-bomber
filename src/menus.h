/* There are menu related functions */

void domenu(struct programstate *psp)            // main menu
{
    static int oldchoice, choice = 0;
    ALLEGRO_COLOR color[5];

    oldchoice = choice;
    if (psp->keys[ALLEGRO_KEY_ESCAPE] == true){
        choice = 4; // put option pointer on exit position
    }else if(psp->keys[ALLEGRO_KEY_ENTER] == true){
        // acccept choice
        switch (choice){
            case 0 : psp->window = 2;
                break;
            case 1 : psp->window = 5;   // game window
                break;
            case 2 : psp->window = 3;
                break;
            case 3 : psp->window = 4;
                break;
            case 4 : psp->quit = true;
                break;
        }
    }else if(psp->keys[ALLEGRO_KEY_UP] == true){
        --choice;// one up
    }else if(psp->keys[ALLEGRO_KEY_DOWN] == true){
        ++choice;// one down
    }
    if (choice < 0)
        choice = 4;
    choice = choice % 5;
    if (choice == 1 && !psp->ingame)    // avoid choosing continue while not ingame
       if (oldchoice == 0)
          choice = 2;
       else
          choice = 0;
    // draw resulting menu
    int i;

    al_clear_to_color(psp->bgcolor);
    al_draw_textf(psp->font, al_map_rgb_f(1,0,0), 0, 0, 0, "CYCLE: %i",psp->cycle);

    // colour options
    for(i=0;i<5;i++){
        if(i == choice)
            color[i] = psp->indicated;
        else
            color[i] = psp->def;
    }
    if(!psp->ingame)
        color[1] = psp->disabled;

    al_draw_textf(psp->font64, psp->def, psp->displaywidth/2-180, 16, 0, "JET BOMBER");

    al_draw_textf(psp->font, color[0], psp->displaywidth/2-100, psp->displayheight/2-96, 0, "NEW GAME");
    al_draw_textf(psp->font, color[1], psp->displaywidth/2-100, psp->displayheight/2-56, 0, "RESUME");
    al_draw_textf(psp->font, color[2], psp->displaywidth/2-100, psp->displayheight/2-16, 0, "SCOREBOARD");
    al_draw_textf(psp->font, color[3], psp->displaywidth/2-100, psp->displayheight/2+24, 0, "CREDITS");
    al_draw_textf(psp->font, color[4], psp->displaywidth/2-100, psp->displayheight/2+64, 0, "EXIT");


    return;
}

void dongame(struct programstate *psp)            // new game menu
{
    int i;
    static int choice = 0;
    static bool edit = false;
    static char input[MAX_NAME_LENGHT];
    int lenght;
    ALLEGRO_COLOR color[2];
    ALLEGRO_COLOR flick;

    if (edit == false)
    {

        if (psp->keys[ALLEGRO_KEY_ESCAPE] == true){
            psp->window = 1; // back to main menu
        }else if(psp->keys[ALLEGRO_KEY_ENTER] == true){
            // acccept choice
            switch (choice){
                case 0 : edit = true;
                         strcpy(input,psp->playername);
                    break;
                case 1 : psp->window = 5;
                         psp->stngame = true;
                    break;
            }
        }else if(psp->keys[ALLEGRO_KEY_UP] == true){
            --choice;// one up
        }else if(psp->keys[ALLEGRO_KEY_DOWN] == true){
            ++choice;// one down
        }
        if (choice < 0)
            choice = 1;
        choice = choice % 2;
    }
    else{                                  // input player name
        if (psp->keys[ALLEGRO_KEY_ESCAPE] == true){
            edit = false; // disable edit
        }
        else if(psp->keys[ALLEGRO_KEY_ENTER] == true){
            // confirm input
            strcpy(psp->playername,input);
            edit = false;
        }else if(psp->keys[ALLEGRO_KEY_BACKSPACE] == true){
            // remove last char from input
            lenght = strlen(input);
            if (lenght > 0)
                input[lenght-1] = '\0';
        }
        else
        {
            lenght = strlen(input);
            if (lenght < MAX_NAME_LENGHT-1){
                for(i=ALLEGRO_KEY_A; i<=ALLEGRO_KEY_SPACE; i++){
                    if (psp->keys[i] == true){
                        // add alfanum char or space
                        if (i >= ALLEGRO_KEY_A && i <= ALLEGRO_KEY_Z){
                            if (psp->keys[ALLEGRO_KEY_LSHIFT] || psp->keys[ALLEGRO_KEY_CAPSLOCK])
                               input[lenght] = i-1+'A';
                            else
                               input[lenght] = i-1+'a';
                            input[lenght+1] = '\0';
                            break;
                        }
                        if (i >= ALLEGRO_KEY_0 && i <= ALLEGRO_KEY_9){
                            if (psp->keys[ALLEGRO_KEY_LSHIFT])
                               switch (i){
                                  case ALLEGRO_KEY_1 :
                                    input[lenght] = '!';
                                    break;
                                  case ALLEGRO_KEY_2 :
                                    input[lenght] = '@';
                                    break;
                                  case ALLEGRO_KEY_3 :
                                    input[lenght] = '#';
                                    break;
                                  case ALLEGRO_KEY_4 :
                                    input[lenght] = '$';
                                    break;
                                  case ALLEGRO_KEY_5 :
                                    input[lenght] = '%';
                                    break;
                                  case ALLEGRO_KEY_6 :
                                    input[lenght] = '^';
                                    break;
                                  case ALLEGRO_KEY_7 :
                                    input[lenght] = '&';
                                    break;
                                  case ALLEGRO_KEY_8 :
                                    input[lenght] = '*';
                                    break;
                                  case ALLEGRO_KEY_9 :
                                    input[lenght] = '(';
                                    break;
                                  case ALLEGRO_KEY_0 :
                                    input[lenght] = ')';
                                    break;
                               }
                            else
                               input[lenght] = i-ALLEGRO_KEY_0+'0';
                            input[lenght+1] = '\0';
                            break;
                        }
                        if (i >= ALLEGRO_KEY_PAD_0 && i <= ALLEGRO_KEY_PAD_9 ){
                            input[lenght] = i-ALLEGRO_KEY_PAD_0+'0';
                            input[lenght+1] = '\0';
                            break;
                        }
                        if (i == ALLEGRO_KEY_SPACE){
                            input[lenght] = 32;
                            input[lenght+1] = '\0';
                            break;
                        }
                    }
                }
            }
        }

    }

    // draw resulting menu

    al_clear_to_color(psp->bgcolor);
    al_draw_textf(psp->font, al_map_rgb_f(1,0,0), 0, 0, 0, "CYCLE: %i",psp->cycle);

    // colour options
    for(i=0;i<2;i++){
        if(i == choice)
            color[i] = psp->indicated;
        else
            color[i] = psp->def;
    }

    al_draw_textf(psp->font64, psp->def, psp->displaywidth/2-180, 16, 0, "NEW GAME");

    al_draw_textf(psp->font, color[0], psp->displaywidth/2-250, psp->displayheight/2-32, 0, "PLAYER NAME:");
    al_draw_textf(psp->font, color[1], psp->displaywidth/2-100, psp->displayheight/2, 0, "BEGIN");

    if(edit == true){
        if ((psp->cycle % (int)FPS) < (FPS / 2))     //  flicker indicating edited text
            flick = al_map_rgb(255,0,0);
        else
            flick = al_map_rgb(0,255,0);

        al_draw_textf(psp->font, flick, psp->displaywidth/2, psp->displayheight/2-32, 0, "%s",input);
    }
    else{
        al_draw_textf(psp->font, psp->def, psp->displaywidth/2, psp->displayheight/2-32, 0, "%s",psp->playername);
    }

    return;
}

void loadconfig(struct programstate *psp)  // load scores to scoreunit array
{
    int i;
    char sc[7];
    char pl[8];

    psp->cfg = al_load_config_file(CFG_FILE);
    if (psp->cfg == NULL)
       return;

    for(i=0;i<10;i++)
    {
       sprintf(sc,"score%i",i);
       sprintf(pl,"player%i",i);
       if (al_get_config_value(psp->cfg,"scores",pl) != NULL)
          //sscanf(al_get_config_value(psp->cfg,"scores",pl),"%s",psp->scores[i].name);
          strncpy(psp->scores[i].name,al_get_config_value(psp->cfg,"scores",pl),MAX_NAME_LENGHT-1);
       if (al_get_config_value(psp->cfg,"scores",sc) != NULL)
          sscanf(al_get_config_value(psp->cfg,"scores",sc),"%i",&(psp->scores[i].score));
    }
}

void saveconfig(struct programstate *psp)  // load scores to scoreunit array
{
    int i;
    char sc[7];
    char pl[8];
    char score[10];

    psp->cfg = al_create_config();
    if (psp->cfg == NULL)
       return;

    al_add_config_section(psp->cfg, "scores");

    for(i=0;i<10;i++)
    {
       sprintf(sc,"score%i",i);
       sprintf(pl,"player%i",i);
       sprintf(score,"%i",psp->scores[i].score);
       al_set_config_value(psp->cfg , "scores", sc, score);
       al_set_config_value(psp->cfg , "scores", pl, psp->scores[i].name);
    }
    al_save_config_file(CFG_FILE, psp->cfg);
}

void insertscore(struct programstate *psp, struct gamestate *gs)
{
    int i, pos = 10;
    scoreunit scu;

    for(i=0;i<10;i++)
       if(gs->points > psp->scores[i].score)
       {
          pos = i;
          break;
       }
    if(pos < 10)
    {
       for (i=9;i>pos;i--)
       {
          psp->scores[i] = psp->scores[i-1];
       }
       psp->scores[pos].score = gs->points;
       strcpy(psp->scores[pos].name,psp->playername);
    }
    psp->lastpos = pos;
    gs->scoreregistered = true;
}

void scoreboard(struct programstate *psp)            // new game menu
{
    int i, displacement = 0;

    char line[32];  // textline

    ALLEGRO_FONT *thefont;
    ALLEGRO_COLOR col;

    if (psp->keys[ALLEGRO_KEY_ESCAPE] == true)
        psp->window = 1; // back to main menu

    // draw resulting menu


    al_clear_to_color(psp->bgcolor);
    al_draw_textf(psp->font, al_map_rgb_f(1,0,0), 0, 0, 0, "CYCLE: %i",psp->cycle);

    al_draw_textf(psp->font64, psp->def, psp->displaywidth/2-180, 16, 0, "SCOREBOARD");

    for(i=0;i<10;i++)
    {
        switch (i)
        {
            case 0:
                thefont = psp->font96;
                break;
            case 1:
                thefont = psp->font64;
                break;
            case 2:
                thefont = psp->font48;
                break;
            default :
                thefont = psp->font;
        }
        if (i == psp->lastpos)
           col = psp->indicated;
        else
           col = psp->def;
        sprintf(line,"%s  %i",psp->scores[i].name,psp->scores[i].score);
        if (psp->scores[i].score != 0)
           al_draw_textf(thefont, col, psp->displaywidth/2-al_get_text_width(thefont, line)/2, psp->displayheight/2-250+displacement, 0, line);
        displacement+=al_get_font_line_height(thefont);
    }
    return;
}

void credits(struct programstate *psp)            // Credits or something like that
{
    int i;

    if (psp->keys[ALLEGRO_KEY_ESCAPE] == true)
        psp->window = 1; // back to main menu

    // draw resulting textwall


    al_clear_to_color(psp->bgcolor);
    al_draw_textf(psp->font, al_map_rgb_f(1,0,0), 0, 0, 0, "CYCLE: %i",psp->cycle);

    al_draw_textf(psp->font64, psp->def, psp->displaywidth/2-140, 16, 0, "CREDITS");

    al_draw_textf(psp->font48, psp->def, psp->displaywidth/2-440, psp->displayheight/2-160, 0, "PROJECT JET BOMBER BY ROBERT LYKO");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-340, psp->displayheight/2-96, 0, "This is game made as PJP II course project.");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-300, psp->displayheight/2-64, 0, "Keys needed to enjoy the game:");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-200, psp->displayheight/2-32, 0, "SPACE - bombs");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-200, psp->displayheight/2, 0, "ARROWS - movement");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-200, psp->displayheight/2+32, 0, "ENTER - confirm");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-200, psp->displayheight/2+64, 0, "ESC - self explanatory");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-400, psp->displayheight/2+96, 0, "Keys F1 - F12 may modify HUD view or gameplay itself");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-200, psp->displayheight/2+128, 0, "");
    al_draw_textf(psp->font, psp->def, psp->displaywidth/2-230, psp->displayheight/2+160, 0, "ESC to go back to main menu");

    return;
}

void error(struct programstate *psp)              // error display actually disposable
{
    al_clear_to_color(al_map_rgb(0,0,255));
    al_draw_textf(psp->font, al_map_rgb_f(1,1,1), psp->displaywidth/2-200, psp->displayheight/2-32, 0, "AN ERROR HAS OCCURED");
    al_draw_textf(psp->font, al_map_rgb_f(1,1,1), psp->displaywidth/2-150, psp->displayheight/2, 0, "WINDOW STATE: %i",psp->window);
    if (psp->keys[ALLEGRO_KEY_ESCAPE] == true){
        psp->window = 1;
    }
    return;
}

void loader(struct programstate *ps)   // load resources
{
    // external resources loader

    ps->dfont = al_create_builtin_font();      // creation of debug purpose font
    ps->font = al_load_font("ahronbd.ttf", -32, 0);      // standard font
    ps->font96 = al_load_font("ahronbd.ttf", -96, 0);      // font size enormous
    ps->font64 = al_load_font("ahronbd.ttf", -64, 0);      // font size huge
    ps->font48 = al_load_font("ahronbd.ttf", -48, 0);      // font size big

    if(!ps->font) {
        fprintf(stderr, "failed to create font!\n");
        ps->font = al_create_builtin_font();
    }

    ps->bomber = al_load_bitmap("gfx\\sprites\\jet_bomber.bmp");        // BOMBER
    if(!ps->bomber)
       ps->bomber = al_create_bitmap(64,64);
    al_convert_mask_to_alpha(ps->bomber,al_map_rgb(255,255,255));

    ps->bomberwreck = al_load_bitmap("gfx\\sprites\\jetwreck.bmp");        // BOMBER WRECK
    if(!ps->bomberwreck)
       ps->bomberwreck = al_create_bitmap(64,64);
    al_convert_mask_to_alpha(ps->bomberwreck,al_map_rgb(255,255,255));

    ps->bomb = al_load_bitmap("gfx\\sprites\\bomb.bmp");                   // BOMB
    if(!ps->bomb)
       ps->bomb = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->bomb,al_map_rgb(255,255,255));

     ps->bullet = al_load_bitmap("gfx\\sprites\\bullet.bmp");                   // MISSILE
    if(!ps->bullet)
       ps->bullet = al_create_bitmap(16,16);
    al_convert_mask_to_alpha(ps->bullet,al_map_rgb(255,255,255));

    ps->target = al_load_bitmap("gfx\\sprites\\bunker.bmp");        // TARGET
    if(!ps->target)
       ps->target = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->target,al_map_rgb(255,255,255));

    ps->targetruin = al_load_bitmap("gfx\\sprites\\ruin.bmp");        // TARGET RUIN
    if(!ps->targetruin)
       ps->targetruin = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->targetruin,al_map_rgb(255,255,255));

    ps->dtarget = al_load_bitmap("gfx\\sprites\\dbunker.bmp");        // DESERT TARGET
    if(!ps->dtarget)
       ps->dtarget = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->dtarget,al_map_rgb(255,255,255));

    ps->dtargetruin = al_load_bitmap("gfx\\sprites\\druin.bmp");        // DESERT TARGET RUIN
    if(!ps->dtargetruin)
       ps->dtargetruin = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->dtargetruin,al_map_rgb(255,255,255));

    ps->enemy = al_load_bitmap("gfx\\sprites\\turret.bmp");        // ENEMY
    if(!ps->enemy)
       ps->enemy = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->enemy,al_map_rgb(255,255,255));

    ps->enemywreck = al_load_bitmap("gfx\\sprites\\turretwreck.bmp");    // ENEMY WRECK
    if(!ps->enemywreck)
       ps->enemywreck = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->enemywreck,al_map_rgb(255,255,255));

    ps->boat = al_load_bitmap("gfx\\sprites\\boat.bmp");    // BOAT
    if(!ps->boat)
       ps->boat = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->boat,al_map_rgb(255,255,255));

    ps->base = al_load_bitmap("gfx\\sprites\\base.bmp");    // BOAT
    if(!ps->base)
       ps->base = al_create_bitmap(32,32);
    al_convert_mask_to_alpha(ps->base,al_map_rgb(255,255,255));

    // sound samples

    ps->shoot = al_load_sample("sfx\\gun.wav" );
    if (!ps->shoot)
      printf( "Audio clip sample not loaded!\n" );

    ps->bombaway = al_load_sample("sfx\\bombsaway.wav" );
    if (!ps->bombaway)
      printf( "Audio clip sample not loaded!\n" );

    ps->boom = al_load_sample("sfx\\blast.wav" );
    if (!ps->boom)
      printf( "Audio clip sample not loaded!\n" );

    ps->hitsound = al_load_sample("sfx\\hit.wav" );
    if (!ps->hitsound)
      printf( "Audio clip sample not loaded!\n" );

    loadconfig(ps);
    loadsprite(ps);
    loadtilemap(ps);
    //printf("Loaded map %s\n",ps->mapfile);


    /*
    printf("\n");
    int i,j;
    for(i=0;i< ps->tilemap.height;i++){
        for(j=0;j< ps->tilemap.width;j++)
           printf("%i",ps->tilemap.tile[i*(ps->tilemap.width)+j]);
        printf("\n");
    }
    printf("\n");
    */

    ps->grgrass = al_load_bitmap("gfx\\tiles\\greengrass.bmp");    // Grass tile
    if(!ps->grgrass)
       ps->grgrass = al_create_bitmap(TILE_DIMENSIONS,TILE_DIMENSIONS);

    ps->yelldes = al_load_bitmap("gfx\\tiles\\yellowdesert.bmp");    // Grass tile
    if(!ps->yelldes)
       ps->yelldes = al_create_bitmap(TILE_DIMENSIONS,TILE_DIMENSIONS);

    ps->noacces = al_load_bitmap("gfx\\tiles\\noacces.bmp");    // No access
    if(!ps->noacces)
       ps->noacces = al_create_bitmap(TILE_DIMENSIONS,TILE_DIMENSIONS);


}
