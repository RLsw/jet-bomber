/*
    Most functions defined here    ....just kidding
*/



void drawgrid(struct gamestate *gs, struct programstate *psp)     // draw green cyber-grid cyberbackground even though there is no cybernetics involved
{

    int i;
    ALLEGRO_COLOR gridColor = al_map_rgb(0,255,0);
    int spacing = TILE_DIMENSIONS; // was 100

    for(i=0;i<(int)ceil((float)psp->displayheight /spacing)+1;++i)
        al_draw_line(0, i*spacing-fmod(gs->camy,spacing), psp->displaywidth, i*spacing-fmod(gs->camy,spacing), gridColor, 2);
    for(i=0;i<(int)ceil((float)psp->displaywidth/spacing)+1;++i)
        al_draw_line(i*spacing-fmod(gs->camx,spacing), 0, i*spacing-fmod(gs->camx,spacing), psp->displayheight, gridColor, 2);
    return;
}

void maketargets(struct gamestate *gs, struct programstate *psp)  // maketargets ewerywhere on tilemap except for 8x8 square on center bottom of tilemap
{  // start player in middle of that square
    int i;

    srand(time(NULL));
    for(i=0;i<MAX_TARGETS;i++)
    {
        gs->target[i].pos.x = (((double)psp->tilemap.width*rand()/RAND_MAX)+psp->displacement.x)*TILE_DIMENSIONS;
        gs->target[i].pos.y = (((double)psp->tilemap.height*rand()/RAND_MAX)+psp->displacement.y)*TILE_DIMENSIONS;
        gs->target[i].life = 100;
        switch (gettiletype(gs->target[i].pos,psp))
        {
           case 1:
              gs->target[i].type=1;
              break;
           case 2:
              gs->target[i].type=2;
              break;
           case 3:
              --i;  // tryagain
              break;
           default:
              gs->target[i].type=1;
              break;
        }
    }
    return;
}

void makeenemys(struct gamestate *gs, struct programstate *psp)   // maketargets ewerywhere on tilemap except for 8x8 square on center bottom of tilemap
{
    int i;

    //srand(time(NULL));
    for(i=0;i<MAX_ENEMYS;i++)
    {
        gs->enemy[i].pos.x = (((double)psp->tilemap.width*rand()/RAND_MAX)+psp->displacement.x)*TILE_DIMENSIONS;
        gs->enemy[i].pos.y = (((double)psp->tilemap.height*rand()/RAND_MAX)+psp->displacement.y)*TILE_DIMENSIONS;
        gs->enemy[i].dir = rand()%360;
        gs->enemy[i].reloading = 0;
        gs->enemy[i].life = 100;
        gs->enemy[i].enable = true;
        switch (gettiletype(gs->enemy[i].pos,psp))
        {
           case 1:
              gs->enemy[i].type=1;
              break;
           case 2:
              gs->enemy[i].type=1;
              break;
           case 3:
              gs->enemy[i].type=2;
              break;
           default:
              gs->enemy[i].type=0;
              break;
        }
    }
    return;
}

double distance(struct pointt orig, struct pointt target)  // return distance between 2 points
{
  return sqrt(pow(orig.x-target.x,2) + pow(orig.y-target.y,2));
}

struct pointt getclosesttarget(struct gamestate *gs)
{
  int i,best;
  float dist, bdist;
  struct pointt ret;
  ret.x = 0.0;
  ret.y = 0.0;

  for (i=0; i<MAX_TARGETS; i++)
    if(gs->target[i].life > 0){
      bdist = distance(gs->ppos, gs->target[i].pos);
      best = i;
      ret = gs->target[best].pos;
      break;
    }
  for (; i<MAX_TARGETS; i++)
  {
    dist = distance(gs->ppos, gs->target[i].pos);
    if ( dist < bdist &&  gs->target[i].life > 0)
    {
       bdist = dist;
       best = i;
       ret = gs->target[best].pos;
    }
  }
  return ret;
}

struct pointt getclosestenemy(struct gamestate *gs)
{
  int i,best;
  float dist, bdist;
  struct pointt ret;
  ret.x = 0.0;
  ret.y = 0.0;

  for (i=0; i<MAX_ENEMYS; i++)
    if(gs->enemy[i].life > 0){
      bdist = distance(gs->ppos, gs->enemy[i].pos);
      best = i;
      ret = gs->enemy[best].pos;
      break;
    }
  for (; i<MAX_ENEMYS; i++)
  {
    dist = distance(gs->ppos, gs->enemy[i].pos);
    if ( dist < bdist  &&  gs->enemy[i].life > 0)
    {
       bdist = dist;
       best = i;
       ret = gs->enemy[best].pos;
    }
  }
  return ret;
}

void gameover(struct gamestate *gs, struct programstate *psp)
{
    char str[16] = {"GAME OVER"};
    int tw, th, border = 16; // text width,text height

    tw = al_get_text_width(psp->font64, str);
    th = al_get_font_ascent(psp->font64);

    al_draw_filled_rounded_rectangle((psp->displaywidth-tw)/2-border, (psp->displayheight-th)/2-border, (psp->displaywidth+tw)/2+border, (psp->displayheight+th)/2+border, border, border, al_map_rgba_f(0,0,0,0.5));
    al_draw_textf(psp->font64, psp->def, (psp->displaywidth-tw)/2, psp->displayheight/2-32, 0, str);

    gs->points = (int)(((MAX_TARGETS - gs->targetsleft) * 5 + (MAX_ENEMYS - gs->enemysleft) * 10));  // POINTS FORMULA
    if(!gs->scoreregistered)
    {
       insertscore(psp,gs);
       saveconfig(psp);
    }
    //printf("%i\n",gs->lastpos);
}

flawlessvictory(struct gamestate *gs, struct programstate *psp)
{
    char str[16], str0[16] = {"*** VICTORY ***"};

    if(!gs->scoreregistered)
    {
       gs->points = (int)(((MAX_TARGETS - gs->targetsleft) * 50 + (MAX_ENEMYS - gs->enemysleft) * 100)*gs->energy/10);  // POINTS FORMULA
       insertscore(psp,gs);
       saveconfig(psp);
    }

    int tw, th, border = 16; // text width,text height

    tw = al_get_text_width(psp->font64, str0);
    th = 64;

    al_draw_filled_rounded_rectangle((psp->displaywidth-tw)/2-border, (psp->displayheight-th)/2-border, (psp->displaywidth+tw)/2+border, (psp->displayheight+th)/2+48+border, border, border, al_map_rgba_f(0,0,0,0.5));

    sprintf(str,"%i pts",gs->points);
    al_draw_textf(psp->font64, al_map_rgb(0,255,0), (psp->displaywidth-tw)/2, (psp->displayheight-th)/2, 0, str0);
    al_draw_textf(psp->font48, al_map_rgb(0,255,0), (psp->displaywidth-al_get_text_width(psp->font48, str))/2, psp->displayheight/2+32, 0, str);

    //printf("%i\n",gs->lastpos);
}

void movebomber(struct gamestate *gs)
{
   gs->ppos.x+=gs->spd/FPS*sin(gs->dir*ALLEGRO_PI/180);
   gs->ppos.y+=gs->spd/FPS*-cos(gs->dir*ALLEGRO_PI/180);
}

void setcamera(struct pointt pt, struct gamestate *gs, struct programstate *psp)
{
  gs->camx = pt.x -psp->displaywidth/2;
  gs->camy = pt.y -psp->displayheight/2;
}

void switcher(struct programstate *psp, struct gamestate *gs)
{

    if((gs->playerdead || gs->playerwon) && (gs->cycle > gs->govercycle + (int) FPS * 2) && ( psp->keys[ALLEGRO_KEY_ENTER] == true || psp->keys[ALLEGRO_KEY_SPACE] == true ))
    {
        psp->window = 3; // to scoreboard
        psp->ingame = false;
    }

    if(psp->keys[ALLEGRO_KEY_F1] == true)   // show list of bombs position and timeleft
        psp->showbombstatus = !psp->showbombstatus;

    if(psp->keys[ALLEGRO_KEY_F2] == true)   // show target life
        psp->showtargetdebug = !psp->showtargetdebug;

    if(psp->keys[ALLEGRO_KEY_F3] == true)   // show enemy position and life
        psp->showenemydebug = false; // !psp->showenemydebug;  // because it slows down program mercilessly and makes something with character input

    if(psp->keys[ALLEGRO_KEY_F3] == true)   // show enemy position and life
        gs->autorelease = !gs->autorelease; // !psp->showenemydebug;  // because it slows down program mercilessly and makes something with character input


    if(psp->keys[ALLEGRO_KEY_F4] == true) // bombsight
       gs->showcrosshair = !gs->showcrosshair;

    static int whoisnext = 0;
    if(psp->keys[ALLEGRO_KEY_F5] == true) // teleport enemys to you
    {
        gs->enemy[whoisnext].pos = gs->ppos;
        whoisnext = (whoisnext + 1) % MAX_ENEMYS;
    }

    static int nextloot = 0;
    if(psp->keys[ALLEGRO_KEY_F9] == true) // teleport enemys to you
    {
        gs->target[nextloot].pos = gs->ppos;
        nextloot = (nextloot + 1) % MAX_TARGETS;
    }

    if(psp->keys[ALLEGRO_KEY_F6] == true) // nasty bullets
        gs->nastymode = !gs->nastymode;

    if(psp->keys[ALLEGRO_KEY_F7] == true) // ghosts take no damage
        gs->ghostmode = !gs->ghostmode;

    if(psp->keys[ALLEGRO_KEY_F8] == true) // don't mind me
        gs->ignoreplayer = !gs->ignoreplayer;

    if(psp->keys[ALLEGRO_KEY_F10] == true) // bombsight
       gs->showdebugbar = !gs->showdebugbar;

    if(psp->keys[ALLEGRO_KEY_F11] == true) // draw pointer
        gs->showpointer = !gs->showpointer;

    if(psp->keys[ALLEGRO_KEY_F12] == true) // draw HUD
        gs->drawhud = !gs->drawhud;
}

void resetgame(struct gamestate *gs, struct programstate *psp)
{
    int i; //

    //other
    gs->drawhud = true;
    gs->showpointer = true;
    gs->showcrosshair = true;
    gs->scoreregistered = false;
    gs->showdebugbar = false;

    //set gamestate to newgame
    gs->playerdead = false;
    gs->playerwon = false;
    gs->drain = true;
    gs->ignoreplayer = true;
    gs->energy = 100;
    gs->cycle = 0;
    gs->dir = 0;
    gs->ppos.x = 0;
    gs->ppos.y = (psp->tilemap.height+psp->displacement.y-4)*TILE_DIMENSIONS;
    gs->points =0;                      // 0 points archieved
    gs->spd = 50;
    gs->bulletcount = 0;
    gs->bombcount = 0;
    gs->bombsdropped = 0;

    gs->targetsleft = MAX_TARGETS;      // all targets to be destroyed
    gs->enemysleft = MAX_ENEMYS;      // all targets to be destroyed
    maketargets(gs,psp);        // generate fresh targets
    makeenemys(gs,psp);         // enemies are born
    for (i=0 ; i < MAX_BOMBS ; i++)
       gs->bomb[i].cycleleft = 0;
    for (i=0 ; i < MAX_BOMBS ; i++)
       gs->xplod[i].cycleleft = 0;
    for (i=0 ; i < MAX_BULLETS ; i++)
       gs->bullet[i].cycleleft = 0;

    if(!psp->tilemap.tile)
       free(psp->tilemap.tile);      // free already allocated tilemap;

    return;
}

void energydrain(int rate,struct gamestate *gs)  // drain rate energy per second
{
   if (gs->cycle % (int)FPS == 0 && !gs->playerdead && gs->cycle != 0)
      gs->energy--;
}

int boomhere(struct gamestate *gs, struct programstate *psp, struct pointt pos, int type)  // set new explosion
{
    int i;

    for(i=0 ; i < MAX_BOMBS ; i++)
    {
        if(gs->xplod[i].cycleleft == 0)
        {
           al_play_sample(psp->boom, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
           gs->xplod[i].cycleleft = (int)FPS * 1;
           gs->xplod[i].pos = pos;
           return 0;
        }
    }
    return -1;
}

double direction(struct pointt orig, struct pointt target)  // direction of (tx,ty) from (x,y) in cartesian coordinate system in degrees
{
    double dx,dy,dir;

    dx = target.x-orig.x;
    dy = target.y-orig.y;

    dir = atan2(dy,dx)*180/ALLEGRO_PI+90;
    if (dir < 0)
        dir += 360;

    return dir;
}

void shoot(struct pointt point,double dir, struct gamestate *gs)    // create missile at point (x,y) in dir direction
{
    int i; // inassociated iterator
    for(i=0;i<MAX_BULLETS;i++)
        if(gs->bullet[i].cycleleft == 0){
            gs->bullet[i].cycleleft = FPS*3;
            gs->bullet[i].pos = point;
            gs->bullet[i].dir = dir;
            gs->bulletcount++;
            break;
        }
    return;
}

int dropbomb(struct pointt position, double dir , struct gamestate *gs, struct programstate *psp)  // bomb insertion function
{
   int i = -1;

   if ( gs->bombcount < MAX_BOMBS && gs->bombdelay == 0)
   {
      for(i=0 ; i<MAX_BOMBS ; ++i)
         {
            if(gs->bomb[i].cycleleft == 0){
               al_play_sample(psp->bombaway, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
               gs->bombsdropped++;
               gs->bombcount++;
               gs->bomb[i].cycleleft = (int)FPS * 1;
               gs->bomb[i].dir = dir;
               gs->bomb[i].pos = position;
               gs->bomb[i].speed = gs->spd;
               break;
            }
         }
      gs->bombdelay = (int)FPS/BOMB_RATE;
   }
   return i;
}

void bomberAI(struct gamestate *gs, struct programstate *psp)  // Artificially artificial intelligence
{
   int i;
        //turn to enemy
        double dir = direction(gs->ppos,getclosestenemy(gs));
        if (dir > gs->dir){
            dir = dir - gs->dir;
            if ( dir > 180 )
                dir = dir -360;
        }
        else{
            dir = -1 * (gs->dir - dir);
            if ( dir < -180 )
                dir = dir + 360;
        }
        if (dir > 0)
            if(fabs(dir) < ROTSPD/FPS)
               gs->dir += dir;
            else
               gs->dir += ROTSPD/FPS;
        else if (dir < 0)
            if(fabs(dir) < ROTSPD/FPS)
               gs->dir += dir;
            else
               gs->dir -= ROTSPD/FPS;

        //al_draw_textf(psp->dfont,al_map_rgb(128,128,0),gs->enemy[baddie].pos.x+20-gs->camx,gs->enemy[baddie].pos.y+30-gs->camy, 0 ,"dir:%.1f",dir);


        if ( gs->dir > 360)
             gs->dir -= 360;

        if ( gs->dir <= 0)
             gs->dir += 360;

        struct pointt t;
        t.x = gs->ppos.x + gs->spd*sin(gs->dir*ALLEGRO_PI/180);
        t.y = gs->ppos.y - gs->spd*cos(gs->dir*ALLEGRO_PI/180);

        if (distance(gs->ppos,getclosestenemy(gs)) < 200 && gs->spd > 66 && gs->spd-ACCELERATION/FPS > MINVEL)
            gs->spd -= ACCELERATION/FPS;
        else if (distance(gs->ppos,getclosestenemy(gs)) > 200 && distance(gs->ppos,getclosestenemy(gs)) < 500)
            if(gs->spd > 400)
              gs->spd -= ACCELERATION/FPS;
            else if (gs->spd < 150)
               gs->spd += ACCELERATION/FPS;
        if (distance(gs->ppos,getclosestenemy(gs)) > 500 && ACCELERATION/FPS+gs->spd < MAXVEL)
            gs->spd += ACCELERATION/FPS;

     for(i=0;i<MAX_ENEMYS;i++)
        if(fabs(distance(gs->enemy[i].pos,t)) < EXPLOSION_RADIUS/3 && gs->bombdelay == 0)       // if aim is good enough
         {
            dropbomb(gs->ppos,gs->dir,gs,psp);    // HERE COMES DEMOCRACY!
            gs->bombdelay = (int)(FPS/BOMB_RATE);
         }
}

void enemylives(int baddie,struct gamestate *gs, struct programstate *psp)  // the soul of your most hatred enemies
{
    if( gs->enemy[baddie].reloading > 0)
        gs->enemy[baddie].reloading--;

    if(distance(gs->ppos,gs->enemy[baddie].pos) < 600 && gs->enemy[baddie].enable) //600
    {
        //turn to bomber
        double dir = direction(gs->enemy[baddie].pos,gs->ppos);
        if (dir > gs->enemy[baddie].dir){
            dir = dir - gs->enemy[baddie].dir;
            if ( dir > 180 )
                dir = dir -360;
        }
        else{
            dir = -1 * (gs->enemy[baddie].dir - dir);
            if ( dir < -180 )
                dir = dir + 360;
        }
        if (dir > 0)
            if(fabs(dir) < TURRET_ROTSPD/FPS)
               gs->enemy[baddie].dir += dir;
            else
               gs->enemy[baddie].dir += TURRET_ROTSPD/FPS;
        else if (dir < 0)
            if(fabs(dir) < TURRET_ROTSPD/FPS)
               gs->enemy[baddie].dir += dir;
            else
               gs->enemy[baddie].dir -= TURRET_ROTSPD/FPS;

        //al_draw_textf(psp->dfont,al_map_rgb(128,128,0),gs->enemy[baddie].pos.x+20-gs->camx,gs->enemy[baddie].pos.y+30-gs->camy, 0 ,"dir:%.1f",dir);


        if ( gs->enemy[baddie].dir > 360)
             gs->enemy[baddie].dir -= 360;

        if ( gs->enemy[baddie].dir <= 0)
             gs->enemy[baddie].dir += 360;

        if(fabs(dir) < 5 && gs->enemy[baddie].reloading == 0)       // if aim is good enough
         {
            shoot(gs->enemy[baddie].pos,gs->enemy[baddie].dir,gs);    // FIRAH!
            al_play_sample(psp->shoot, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
            gs->enemy[baddie].reloading = FPS / FIRE_RATE;
         }
    }

    return;
}

void game(struct programstate *psp)              // game itself
{
    int i,j; // insignificant iterator
    static struct gamestate gs = {0};
    if (psp->stngame == true)
    {
        resetgame(&gs,psp);     // reset game
        psp->stngame = false;
        psp->ingame = true;
    }

    al_clear_to_color(al_map_rgb(0,0,0));

    if (psp->keys[ALLEGRO_KEY_ESCAPE] == true){
        psp->window = 1;
    }

    // READ SOME INPUT (FOR DEBUG)

    switcher(psp,&gs); // switchers

    if (gs.energy <= 0 && gs.playerdead == false)  // make player dead
    {
        gs.govercycle = gs.cycle;
        gs.playerdead = true;
    }
    else if (gs.energy > 0 && gs.playerwon == false && gs.targetsleft == 0 && gs.enemysleft == 0)
    {
        gs.playerwon = true;
        gs.govercycle = gs.cycle;
    }


    //  ENERGY DRAIN
    if(gs.drain)
       energydrain(1,&gs);

    //  ENEMYS AI

    if (gs.cycle == (int)FPS * 3)
        gs.ignoreplayer = false;

    if (!gs.ignoreplayer && !gs.playerdead)
        for(i=0;i<MAX_ENEMYS;i++)
        {
            enemylives(i,&gs,psp);
        }

    // MISSILES MOVEMENT

    for(i=0 ; i<MAX_BULLETS ; i++)
    {
       if ( !gs.ghostmode && distance(gs.bullet[i].pos, gs.ppos) <= BOMBER_RADIUS && gs.bullet[i].cycleleft != 0 && !gs.playerdead)
       {
           gs.bullet[i].cycleleft = 0;
           al_play_sample(psp->hitsound, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
           gs.energy -= 10;
           gs.bulletcount--;
           continue;
       }

        // nasty?

        if (gs.nastymode)
        {
            double dir = direction(gs.bullet[i].pos,gs.ppos);
            if (dir > gs.bullet[i].dir)
            {
                dir = dir - gs.bullet[i].dir;
                if ( dir > 180 )
                    dir = dir -360;
            }
            else{
                dir = -1 * (gs.bullet[i].dir - dir);
                if ( dir < -180 )
                    dir = dir + 360;
            }
            if (dir > 0)
                gs.bullet[i].dir += TURRET_ROTSPD/FPS;
            else if (dir < 0)
                gs.bullet[i].dir -= TURRET_ROTSPD/FPS;

            if ( gs.bullet[i].dir > 360)
                gs.bullet[i].dir -= 360;

            if ( gs.bullet[i].dir <= 0)
                gs.bullet[i].dir += 360;

        }

       if(gs.bullet[i].cycleleft > 0)
       {
          gs.bullet[i].pos.x+=MISSILE_VELOCITY/FPS*sin(gs.bullet[i].dir*ALLEGRO_PI/180);
          gs.bullet[i].pos.y+=MISSILE_VELOCITY/FPS*-cos(gs.bullet[i].dir*ALLEGRO_PI/180);
          gs.bullet[i].cycleleft--;
          if (gs.bullet[i].cycleleft == 0)
            gs.bulletcount--;
       }
    }

    //  BOMBER MECHANICS
    if (!gs.playerdead)
    {
      // ROTATION
      //bomberAI(&gs,psp);
      if(al_key_down(psp->kb,ALLEGRO_KEY_RIGHT))
         gs.dir+=ROTSPD/FPS;

      if(al_key_down(psp->kb,ALLEGRO_KEY_LEFT))
         gs.dir-=ROTSPD/FPS;

      if(gs.dir >= 360)
          gs.dir-=360;
      if(gs.dir < 0)
          gs.dir+=360;

      // ACCELERATION
      if(al_key_down(psp->kb,ALLEGRO_KEY_UP) && gs.spd < MAXVEL)
      {
         gs.spd+=ACCELERATION/FPS;
         if(gs.spd>MAXVEL)
            gs.spd=MAXVEL;
      }
      if(al_key_down(psp->kb,ALLEGRO_KEY_DOWN) && gs.spd > MINVEL)
      {
         gs.spd-=ACCELERATION/FPS;
         if(gs.spd<MINVEL)
            gs.spd=MINVEL;
      }

      // BOMBER MOVEMENT
      movebomber(&gs);
      setcamera(gs.ppos,&gs,psp);
      //setcamera(getclosestenemy(&gs),&gs);

      // BOMBING
        // autorelease

        if(gs.autorelease){
        struct pointt t;
        t.x = gs.ppos.x + gs.spd*sin(gs.dir*ALLEGRO_PI/180);
        t.y = gs.ppos.y - gs.spd*cos(gs.dir*ALLEGRO_PI/180);
           for(i=0;i<MAX_ENEMYS;i++)
              if(((fabs(distance(gs.enemy[i].pos,t)) < EXPLOSION_RADIUS/4 && gs.enemy[i].life > 0) || (fabs(distance(gs.target[i].pos,t)) < EXPLOSION_RADIUS/4 && gs.target[i].life > 0)) && gs.bombdelay == 0)       // if aim is good enough
              {
                printf("DISTANCE: %f\n",fabs(distance(gs.enemy[i].pos,t)));
                dropbomb(gs.ppos,gs.dir,&gs,psp);    // HERE COMES DEMOCRACY!
                gs.bombdelay = (int)(FPS/BOMB_RATE);
              }
        }

        if(al_key_down(psp->kb,ALLEGRO_KEY_SPACE))
          dropbomb(gs.ppos,gs.dir,&gs,psp);

        if(gs.bombdelay > 0) // make bomb transient
           gs.bombdelay --;
      }
      else  // BREAKING
      {
         if (gs.spd > 0 && gs.spd > ACCELERATION/FPS)
         {
            gs.spd -= ACCELERATION/FPS;
            movebomber(&gs);
         }
         else
            gs.spd = 0.0;
      }

        for(i=0;i<MAX_BOMBS;i++)
        {
           if(gs.bomb[i].cycleleft > 0)
           {
              gs.bomb[i].pos.x+=gs.bomb[i].speed/FPS*sin(gs.bomb[i].dir*ALLEGRO_PI/180);
              gs.bomb[i].pos.y+=gs.bomb[i].speed/FPS*-cos(gs.bomb[i].dir*ALLEGRO_PI/180);
              gs.bomb[i].cycleleft--;
              if(gs.bomb[i].cycleleft == 0)
              {
                 gs.bombcount--;
                 boomhere(&gs,psp,gs.bomb[i].pos,1);
              }
           }
        }

        // BLOWING

        for(i=0;i< MAX_BOMBS;i++)
        {
           if(gs.xplod[i].cycleleft > 0){

              // DEAL DAMAGE
              for (j=0;j<MAX_ENEMYS;j++){
                if (distance(gs.xplod[i].pos,gs.enemy[j].pos) < EXPLOSION_RADIUS && gs.enemy[j].life > 0 && gs.xplod[i].cycleleft == (int)FPS )
                {
                   printf("BOOM : %.2f\n",200 * powf((EXPLOSION_RADIUS - distance(gs.xplod[i].pos,gs.enemy[j].pos))/EXPLOSION_RADIUS,2));
                   gs.enemy[j].life  -= 200 * powf((EXPLOSION_RADIUS - distance(gs.xplod[i].pos,gs.enemy[j].pos))/EXPLOSION_RADIUS,2);
                   if(gs.enemy[j].life <= 0){
                      gs.enemy[j].enable = false;
                      --gs.enemysleft;
                      if (!gs.playerdead)
                         gs.energy += 10;
                   }
                }
              }
              for (j=0;j<MAX_TARGETS;j++){
                if (distance(gs.xplod[i].pos,gs.target[j].pos) < EXPLOSION_RADIUS && gs.target[j].life > 0 && gs.xplod[i].cycleleft == (int)FPS )
                {
                   printf("POOF : %.2f\n",200 * powf((EXPLOSION_RADIUS - distance(gs.xplod[i].pos,gs.target[j].pos))/EXPLOSION_RADIUS,2));
                   gs.target[j].life -= 200 * powf((EXPLOSION_RADIUS - distance(gs.xplod[i].pos,gs.target[j].pos))/EXPLOSION_RADIUS,2);
                   if(gs.target[j].life <= 0){
                      gs.targetsleft--;
                      if (!gs.playerdead)
                         gs.energy += 5;
                   }
                }
              }
              gs.xplod[i].cycleleft--;
           }
        }

    //  DRAWING PART

    // draw background
    drawgrid(&gs,psp);
    heniekthetiler(psp,&gs);

    //drawsprite(psp,&gs, 100, 100);

    // draw targets

    for(i=0;i< MAX_TARGETS;i++)
       if(gs.target[i].life > 0)
         switch (gs.target[i].type)
         {
           case 1:
             al_draw_rotated_bitmap(psp->dtarget, al_get_bitmap_width(psp->dtarget)/2,al_get_bitmap_height(psp->dtarget)/2, gs.target[i].pos.x-gs.camx, gs.target[i].pos.y-gs.camy,0, 0);
             break;
           case 2:
             al_draw_rotated_bitmap(psp->target, al_get_bitmap_width(psp->target)/2,al_get_bitmap_height(psp->target)/2, gs.target[i].pos.x-gs.camx, gs.target[i].pos.y-gs.camy,0, 0);
             break;
         }
       else
         switch (gs.target[i].type)
         {
           case 1:
             al_draw_rotated_bitmap(psp->dtargetruin, al_get_bitmap_width(psp->dtargetruin)/2,al_get_bitmap_height(psp->dtargetruin)/2, gs.target[i].pos.x-gs.camx, gs.target[i].pos.y-gs.camy,0, 0);
             break;
           case 2:
             al_draw_rotated_bitmap(psp->targetruin, al_get_bitmap_width(psp->targetruin)/2,al_get_bitmap_height(psp->targetruin)/2, gs.target[i].pos.x-gs.camx, gs.target[i].pos.y-gs.camy,0, 0);
             break;
          }


    // draw enemies

    for(i=0;i< MAX_ENEMYS;i++)
    {
       switch (gs.enemy[i].type)
       {
          case 1:
            al_draw_rotated_bitmap(psp->base, al_get_bitmap_width(psp->base)/2,al_get_bitmap_height(psp->base)/2, gs.enemy[i].pos.x-gs.camx, gs.enemy[i].pos.y-gs.camy,0, 0);
            break;
          case 2:
            al_draw_rotated_bitmap(psp->boat, al_get_bitmap_width(psp->boat)/2,al_get_bitmap_height(psp->boat)/2, gs.enemy[i].pos.x-gs.camx, gs.enemy[i].pos.y-gs.camy,0, 0);
            break;
          default:
            break;
       }
       if(gs.enemy[i].life > 0)
           al_draw_rotated_bitmap(psp->enemy, al_get_bitmap_width(psp->enemy)/2,al_get_bitmap_height(psp->enemy)/2, gs.enemy[i].pos.x-gs.camx, gs.enemy[i].pos.y-gs.camy,gs.enemy[i].dir*ALLEGRO_PI/180, 0);
       else
           al_draw_rotated_bitmap(psp->enemywreck, al_get_bitmap_width(psp->enemywreck)/2,al_get_bitmap_height(psp->enemywreck)/2, gs.enemy[i].pos.x-gs.camx, gs.enemy[i].pos.y-gs.camy,gs.enemy[i].dir*ALLEGRO_PI/180, 0);
    }
    // draw bullets or missiles :)
    for(i=0;i< MAX_BULLETS;i++)
       if(gs.bullet[i].cycleleft != 0)
          al_draw_rotated_bitmap(psp->bullet, al_get_bitmap_width(psp->bullet)/2,al_get_bitmap_height(psp->bullet)/2, gs.bullet[i].pos.x-gs.camx, gs.bullet[i].pos.y-gs.camy,gs.bullet[i].dir*ALLEGRO_PI/180, 0);

    // draw explosions
    for(i=0;i< MAX_BOMBS;i++)
      if(gs.xplod[i].cycleleft > 0)
         al_draw_filled_circle(gs.xplod[i].pos.x-gs.camx, gs.xplod[i].pos.y-gs.camy,(FPS*1-gs.xplod[i].cycleleft)/FPS*EXPLOSION_RADIUS ,al_map_rgba(gs.xplod[i].cycleleft*255/FPS,gs.xplod[i].cycleleft*255/FPS,0,gs.xplod[i].cycleleft*255/FPS));

    // draw bombs
    for(i=0;i< MAX_BOMBS;i++)
       if(gs.bomb[i].cycleleft != 0)
          al_draw_scaled_rotated_bitmap(psp->bomb, al_get_bitmap_width(psp->bomb)/2,al_get_bitmap_height(psp->bomb)/2, gs.bomb[i].pos.x-gs.camx, gs.bomb[i].pos.y-gs.camy,(float)gs.bomb[i].cycleleft*0.5/FPS+0.5,(float)gs.bomb[i].cycleleft*0.5/FPS+0.5,gs.bomb[i].dir*ALLEGRO_PI/180, 0);


    // draw plane
    if(gs.energy > 0)
       al_draw_rotated_bitmap(psp->bomber,al_get_bitmap_width(psp->bomber)/2,al_get_bitmap_height(psp->bomber)/2,gs.ppos.x-gs.camx,gs.ppos.y-gs.camy,gs.dir*ALLEGRO_PI/180,0);
    else
       al_draw_rotated_bitmap(psp->bomberwreck,al_get_bitmap_width(psp->bomberwreck)/2,al_get_bitmap_height(psp->bomberwreck)/2,gs.ppos.x-gs.camx,gs.ppos.y-gs.camy,gs.dir*ALLEGRO_PI/180,0);


    if(gs.drawhud)      // DRAW HUD
       drawhudf(psp,&gs);

    gs.cycle += 1; // last cycle processed
    return;
}

void drawhudf(struct programstate *psp, struct gamestate *gs)    // HUD drawer
{
    int i; // how pity
    char hud[255];

    if(psp->showbombstatus == true){
      al_draw_filled_rectangle( psp->displaywidth*2/3-10, 90, psp->displaywidth*2/3+232, 110+MAX_BOMBS*10, al_map_rgba_f(0,0,0,0.5));
      for(i=0;i<MAX_BOMBS;i++) // BOMBS STATUS
         al_draw_textf(psp->dfont,al_map_rgb(128,128,0),psp->displaywidth*2/3,100+10*i, 0 ,"X:%.1f Y:%.1f HEIGHT:%d",gs->bomb[i].pos.x,gs->bomb[i].pos.y,gs->bomb[i].cycleleft);
      }

    /*if(psp->showbulletstatus == true)  // slows down game hence don't use it.
    {
      al_draw_textf(psp->dfont,al_map_rgb(196,196,0),10,30, 0 ,"BULLETCOUNT %d",gs->bulletcount);
      for(i=0;i<MAX_BULLETS;i++) // BULLET STATUS
         al_draw_textf(psp->dfont,al_map_rgb(128,128,0),10,40+10*i, 0 ,"#%d X:%.1f Y:%.1f LIFELEFT:%d",i,gs->bullet[i].x,gs->bullet[i].y,gs->bullet[i].cycleleft);
    }*/

    char enemystr[32];
    if (psp->showenemydebug)  // ENEMY DATA
       for(i=0;i<MAX_ENEMYS;i++){
          sprintf(enemystr,"direction:%.1f life:%d",gs->enemy[i].dir,gs->enemy[i].life);
          al_draw_filled_rectangle( gs->enemy[i].pos.x+20-gs->camx-4, gs->enemy[i].pos.y+20-gs->camy-4, gs->enemy[i].pos.x+20-gs->camx-4+200, gs->enemy[i].pos.y+20-gs->camy+14, al_map_rgba_f(0,0,0,0.5));
          al_draw_textf(psp->dfont,al_map_rgb(0,196,0),gs->enemy[i].pos.x+20-gs->camx,gs->enemy[i].pos.y+20-gs->camy, 0 ,enemystr);
       }

    if (psp->showtargetdebug)  // TARGET DATA
       for(i=0;i<MAX_TARGETS;i++){
          al_draw_filled_rectangle( gs->target[i].pos.x+20-gs->camx-4, gs->target[i].pos.y+20-gs->camy-4, gs->target[i].pos.x+20-gs->camx+80, gs->target[i].pos.y+20-gs->camy+14, al_map_rgba_f(0,0,0,0.5));
          al_draw_textf(psp->dfont,al_map_rgb(0,196,0),gs->target[i].pos.x+20-gs->camx,gs->target[i].pos.y+20-gs->camy, 0 ,"life: %d",gs->target[i].life);
       }

    if (gs->showcrosshair) // CROSSHAIR
    {
        float x,y;
        x = gs->ppos.x-gs->camx + gs->spd*sin(gs->dir*ALLEGRO_PI/180);
        y = gs->ppos.y-gs->camy - gs->spd*cos(gs->dir*ALLEGRO_PI/180);
        al_draw_line(x-12, y-12, x+12, y+12,al_map_rgb(255,255,0), 4);
        al_draw_line(x-12, y+12, x+12, y-12,al_map_rgb(255,255,0), 4);
        al_draw_line(x-10, y-10, x+10, y+10,al_map_rgb(0,255,0), 2);
        al_draw_line(x-10, y+10, x+10, y-10,al_map_rgb(0,255,0), 2);
    }

    // TARGETINDICATOR
    if (gs->showpointer)
    {
       double tgt = direction(gs->ppos,getclosesttarget(gs));   // there target
       double enemy = direction(gs->ppos,getclosestenemy(gs));  // there enemy

       float x,y,x2,y2;
       if (gs->targetsleft != 0)
       {
         x = gs->ppos.x-gs->camx + 64*sin(tgt*ALLEGRO_PI/180);
         y = gs->ppos.y-gs->camy - 64*cos(tgt*ALLEGRO_PI/180);
         x2 = gs->ppos.x-gs->camx + 96*sin(tgt*ALLEGRO_PI/180);
         y2 = gs->ppos.y-gs->camy - 96*cos(tgt*ALLEGRO_PI/180);
         al_draw_line(x, y, x2, y2,al_map_rgb(0,255,0), 2);
       }
       if (gs->enemysleft != 0)
       {
         x = gs->ppos.x-gs->camx + 96*sin(enemy*ALLEGRO_PI/180);
         y = gs->ppos.y-gs->camy - 96*cos(enemy*ALLEGRO_PI/180);
         x2 = gs->ppos.x-gs->camx + 128*sin(enemy*ALLEGRO_PI/180);
         y2 = gs->ppos.y-gs->camy - 128*cos(enemy*ALLEGRO_PI/180);
         al_draw_line(x, y, x2, y2,al_map_rgb(255,0,0), 2);
       }
    }


    i=0;

    if (psp->showbombstatus){
       al_draw_textf(psp->dfont,al_map_rgb(0,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"BOMBS STAT");
       i++;
    }
    if (psp->showtargetdebug){
       al_draw_textf(psp->dfont,al_map_rgb(0,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"TGT DEBUG");
       i++;
    }
    if (psp->showenemydebug){
       al_draw_textf(psp->dfont,al_map_rgb(0,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"ENEMY DEBUG");
       i++;
    }
    if (gs->autorelease){
       al_draw_textf(psp->dfont,al_map_rgb(0,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"AUTORELEASE");
       i++;
    }
    if (gs->showcrosshair){
       al_draw_textf(psp->dfont,al_map_rgb(0,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"BOMBSIGHT");
       i++;
    }
    if (gs->nastymode){
       al_draw_textf(psp->dfont,al_map_rgb(255,0,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"NASTY!!!");
       i++;
    }
    if (gs->ghostmode){
       al_draw_textf(psp->dfont,al_map_rgb(255,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"GHOSTMODE");
       i++;
    }
    if (gs->showpointer){
       al_draw_textf(psp->dfont,al_map_rgb(0,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"TARGET POINTER");
       i++;
    }
    if (gs->ignoreplayer){
       al_draw_textf(psp->dfont,al_map_rgb(255,255,0),10,psp->displayheight/2+i*10,ALLEGRO_ALIGN_LEFT ,"DON'T MIND ME");
       i++;
    }

    if(gs->showdebugbar){
       //psp->tinted = al_create_sub_bitmap(psp->displayBuffer, 0, 0, psp->displaywidth, 24);  // why I didn't I used tinted rectangles
       //al_draw_tinted_bitmap(psp->tinted, al_map_rgba_f(0,0,0,0.5), 0, 0, 0);
       al_draw_filled_rectangle(0, 0, psp->displaywidth, 24, al_map_rgba_f(0,0,0,0.5));
       sprintf(hud,"x = %7.1lf | y = %7.1lf | angle = %5.1lf | velocity = %6.1lf  | cycle = %5d | bullets = %3d | window = %1d | bombdelay %2d | tile (%d,%d)[%d]",gs->ppos.x,gs->ppos.y,gs->dir,gs->spd,gs->cycle,gs->bulletcount ,psp->window, gs->bombdelay,(int)floor(gs->ppos.x/TILE_DIMENSIONS),(int)floor(gs->ppos.y/TILE_DIMENSIONS),gettiletype(gs->ppos,psp));
       al_draw_textf(psp->dfont,al_map_rgb(0,255,0),10,10,ALLEGRO_ALIGN_LEFT ,hud);
    }

    //psp->tinted = al_create_sub_bitmap(psp->displayBuffer, 0, 0, psp->displaywidth, 64);   // BTW it worked howewer code looks incorrect - 0,0 ?
    //psp->tinted = al_create_sub_bitmap(psp->displayBuffer, 0, psp->displayheight-64, psp->displaywidth, 64);
    //al_draw_tinted_bitmap(psp->tinted, al_map_rgba_f(0,0,0,0.5), 0, psp->displayheight-64, 0);
    al_draw_filled_rectangle( 0, psp->displayheight-64, psp->displaywidth, psp->displayheight, al_map_rgba_f(0,0,0,0.5));
    al_draw_textf(psp->font,al_map_rgb(0,255,0),10,psp->displayheight-42,ALLEGRO_ALIGN_LEFT ,"ENEMIES: %3d  TARGETS: %3d BOMBS USED: %d",gs->enemysleft, gs->targetsleft, gs->bombsdropped);

    char hud2[16];
    sprintf(hud2,"ENERGY: %4d", gs->energy);
    al_draw_textf(psp->font,al_map_rgb(0,255,0),psp->displaywidth-al_get_text_width(psp->font,hud2)-10,psp->displayheight-42,ALLEGRO_ALIGN_LEFT ,hud2);

    if (gs->targetsleft==0 && gs->enemysleft==0)
       flawlessvictory(gs,psp);

    if (gs->playerdead)
       gameover(gs,psp);

}
